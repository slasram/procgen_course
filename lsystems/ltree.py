import collections as col
from base import Renderer, Rule, Cmd, DrawCmd, RuleMgr
import random
import turtle

def Coeff(amplitude):
    while True:
        yield 1 + ((random.random() - 0.5) * amplitude)

def Same():
    while True:
        yield 1

class Splitter(Rule):
    def __init__(self, angle, dist, variation=None):
        Rule.__init__(self)
        self.dist  = dist
        self.angle = angle
        self.variation = variation or Same()

    def match(self, cmd):
        return cmd.name == "F"

    def apply(self, cmd):
        angle = self.angle * self.variation.next()
        left_angle = angle * random.random()
        right_angle = angle - left_angle
        #==
        dist = self.dist * self.variation.next()
        d1 = dist * random.random()
        d2 = dist - d1
        shortest, longest = sorted((d1, d2))
        if left_angle < right_angle:
            left_dist, right_dist = longest, shortest
        if left_angle >= right_angle:
            right_dist, left_dist = longest, shortest
        #==
        
        return [ Cmd('FF', cmd.param),
                 Cmd('PushState', None),
                 Cmd('TL', left_angle),
                 Cmd('F',  left_dist),
                 Cmd('PopState', None),
                 Cmd('TR', right_angle),
                 Cmd('F',  right_dist),
                 ]

class MinorBranch(Rule):
    def __init__(self, step):
        self.step = step

    def match(self, cmd):
        return cmd.name == "F"

    def apply(self, cmd):
        quotient, remainder = divmod( cmd.param,  self.step)
        nb     =  int( quotient + ( 1 if remainder != 0 else 0) )
        last   = nb -1 
        result = [
                #Cmd( 'PushState', None),
                #cmd,
                #Cmd( 'PopState', None),
                ]
        for idx, step in enumerate( (self.step if i!= last else remainder ) for i in range(nb)):
            side ='TL'if idx%2==0 else 'TR'
            result.extend([ 
                            Cmd( 'PushState', None),
                            Cmd( side, 20 ),
                            Cmd( 'L', step),
                            Cmd( 'PopState', None),
                            Cmd( 'FF', step),
                            ] )
        return result

class Leaf(Rule):
    def __init__(self, step):
        self.step = step

    def match(self, cmd):
        return cmd.name in ( "L" )

    def apply(self, cmd):
        result = [  
                    Cmd( 'PushState', None),
                    Cmd( 'BeginFill', "#00ff00" ),
                    Cmd( 'TR', 30 ),
                    Cmd( 'F', self.step),
                    Cmd( 'TL', 60 ),
                    Cmd( 'F', self.step),
                    Cmd( 'TL', 120 ),
                    Cmd( 'F', self.step),
                    Cmd( 'TL', 60 ),
                    Cmd( 'EndFill', None ),
                    Cmd( 'PopState', None),
                    ] 
        return result



class TurtleCmd(DrawCmd):
    def match(self, cmd):
        return cmd.name in self.names
    def execute(self,turtle, cmd, context): 
        pass

class Forward(TurtleCmd):
    names = ( 'FF', 'F', 'L')
    def execute(self, cmd, context):
        print 'here'
        turtle.forward(cmd.param)

class TurnLeft(TurtleCmd):
    names = ('TL',)
    def execute(self, cmd, context):
        turtle.left( cmd.param)

class TurnRight(TurtleCmd):
    names = ('TR',)
    def execute(self, cmd, context):
        turtle.right( cmd.param)

class PushState(TurtleCmd):
    names = ('PushState',)
    def execute(self, cmd, context):
        context.pos.append( (turtle.pos(), turtle.heading() ) )

class PopState(TurtleCmd):
    names = ('PopState',)
    def execute(self, cmd, context):
        turtle.penup()
        pos, heading = context.pos.pop()
        turtle.setpos( pos )
        turtle.setheading( heading)
        turtle.pendown()

class BeginFill(TurtleCmd):
    names = ('BeginFill',)
    def execute(self, cmd, context):
        context.color.append( turtle.fillcolor() )
        color = cmd.param
        turtle.fillcolor(color)
        turtle.begin_fill()

class EndFill(TurtleCmd):
    names = ('EndFill',)
    def execute(self, cmd, context):
        color = context.color.pop()
        turtle.fillcolor(color)
        turtle.end_fill()

Context = col.namedtuple('Context', 'pos  color')

class LRenderer(Renderer):
    def __init__(self, drawing_cmds):
        self.drawing_cmds = drawing_cmds

    def get_context(self):
        return Context( [], [] )

    def pre_draw(self, context):
        turtle.goto(0,-150)
        turtle.setheading(90)
        turtle.clear()
        turtle.tracer(8,0)
        turtle.hideturtle()

    def post_draw(self, context):
        turtle.update()


default_renderer = LRenderer( [ Forward(),
                               TurnLeft(),
                               TurnRight(),
                               PushState(),
                               PopState(),
                               BeginFill(),
                               EndFill()
                               ] )


def test():
    start = [ Cmd( 'F', 100) ]
    rulemgr = RuleMgr( [ Splitter(60, 30) ] )
    rulemgr2 = RuleMgr( [Leaf(5), MinorBranch(10) ] )
    cmds = rulemgr.apply( start, 7)
    cmds = rulemgr2.apply( cmds, 1)
    print cmds
    default_renderer.draw( cmds )

